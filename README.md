# The Super Sampling App

Draw the best samples.

## Installation

You'll need the `ssampling` library to run the ShinyApp. 

```r
devtools::install_gitlab("marberts/sampling-library")
```

## Usage

```r
shiny::runUrl("https://gitlab.com/marberts/sampling-app2/-/archive/master/sampling-app2-master.tar.gz")
```
