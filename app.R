#---- Libraries ----    
library(shiny)
library(ssampling)

#---- UI ----
ui <- fluidPage(
    titlePanel("The Super Sampling App"),
    sidebarLayout(
        sidebarPanel(
            sliderInput("size", "Sample Size", 0, nrow(mtcars), value = 1, step = 1),
        ),
        mainPanel(
           plotOutput("plot")
        )
    )
)

#---- Server ----
server <- function(input, output) {
    output$plot <- renderPlot({
        x <- mtcars[sample_srs(nrow(mtcars), input$size)[[1]], ]
        with(x, plot(mpg, disp))
    })
}

#---- Run ----
shinyApp(ui = ui, server = server)
